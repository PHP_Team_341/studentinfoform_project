<?php
	require 'connEventUser.php';

	if(isset($_POST['login'])) {
		$errMsg = '';

		// Get data from FORM
		$username = $_POST['event_user_name'];
		$password = $_POST['event_user_password'];

		if($username == '')
			$errMsg = 'Enter username';
		if($password == '')
			$errMsg = 'Enter password';

		if($errMsg == '') {
			try {
				$stmt = $conn->prepare('SELECT event_user_id ,event_user_name,event_user_password FROM event_user WHERE event_user_name = :username');
				$stmt->execute(array(
					':username' => $username
					));
				$data = $stmt->fetch(PDO::FETCH_ASSOC);

				if($data == false){
					$errMsg = "User $username not found.";
				}
				else {
					if($password == $data['event_user_password']) {     ////
						$_SESSION['validUser'] =$data['event_user_name'];
						$_SESSION['event_user_password'] = $data['event_user_password'];
						header('Location:Admin.php');
						exit;
					}
					else
						$errMsg = 'Password not match.';
				}
			}
			catch(PDOException $e) {
				$errMsg = $e->getMessage();
			}
		}
	}
?>
<html>
<head><title>Login</title></head>
	<style>
	html, body {
		margin: 1px;
		border: 0;
	}
	</style>
<body>
	<div align="center">
		<div style=" border: solid 1px #006D9C; " align="center">
			
			<div style="background-color:#006D9C; color:#FFFFFF; padding:10px;"><b>Login</b></div>
			<div style="margin: 15px">
				<form action="" method="post">
				     <label >Username:</label>
					<input type="text" name="event_user_name" value="<?php if(isset($_POST['event_user_name'])) echo $_POST['event_user_name'] ?>"  class="box"/><br /><br />
					
					 <label >Password:</label>
					<input type="password" name="event_user_password" value="<?php if(isset($_POST['event_user_password'])) echo $_POST['event_user_password'] ?>"  class="box" /><br/><br />
					
					<input type="submit" name='login' value="Login" class='submit'/><br />
					
					
					<?php
				if(isset($errMsg)){
					echo '<div style="color:#FF0000;text-align:center;font-size:17px;">'.$errMsg.'</div>';
				}
			?>

				</form>
			</div>
		</div>
	</div>
</body>
</html>
